
<a href="https://www.linkpicture.com/view.php?img=LPic638b7a4ebfa69801775351"><img src="https://www.linkpicture.com/q/LPic638b7a4ebfa69801775351.png" type="image"></a>

<details>
    <summary>🎯Dart Basics</summary>
    <ul>
    <details>
        <summary>🧑🏾‍💻Operations</summary>
        <ul>
            <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/1.1_operations/bin/arithmetic_op.dart">1. Arithmetic Operations</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/1.1_operations/bin/null_aware.dart">2. Null Aware</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/1.1_operations/bin/type_test.dart">3. Type Test</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/Dart_Learning_Path/1_Basics/1.1_operations/bin/math_operations.dart">4. Math Lib Functions</a> </li>
        </ul>
    </details>    
    <details>
        <summary>👨🏻‍🏫Variables</summary> 
        <ul>
            <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/1_variables/bin/const_final.dart">1. Const and Final</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/1_variables/bin/dynamic_null.dart">2. Dynamic null</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/1_variables/bin/main.dart">3. Main</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/1_variables/bin/object.dart">4. Object</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/1_variables/bin/string.dart">5. String class</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/Dart_Learning_Path/1_Basics/1_variables/bin/string_concat.dart">6. String Concat</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/Dart_Learning_Path/1_Basics/1_variables/bin/object_dynamic_types.dart">7. Object And Dynamic Data Type</a> </li>
        </ul> 
    </details>    
    <details>
        <summary>🚩Conditionals</summary>     
        <ul>
            <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/2_conditionals/bin/assert.dart">1. Assert</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/2_conditionals/bin/main.dart">2. Main</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/2_conditionals/bin/ternary_condit.dart">3. Ternary Conditionals</a> </li>
        </ul> 
    </details>    
    <details>
        <summary>😵‍💫Loops</summary>    
        <ul>
            <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/4_loops/bin/break_continue.dart">1. Break and Continue</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/4_loops/bin/foreach.dart">2. Foreach</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/4_loops/bin/loop.dart">3. Loop</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/4_loops/bin/main.dart">4. Main</a> </li>
        </ul> 
    </details>
    <details>
        <summary>📦Functions</summary>    
        <ul>
            <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/6_functions/bin/anonym_func.dart">1. Anonym  Functions</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/6_functions/bin/main.dart">2. Main</a> </li>
        </ul> 
    </details> 
    <details>
        <summary>🎨Collections</summary>    
        <ul>
            <details>
                <summary>📋Lists</summary>
                <ul>
                    <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/list/list.dart">1. List</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/list/list_upper.dart">2. List_Upper_Level</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/list/main.dart">3. Main</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/6_functions/bin/anonym_func.dart">1. Anonym  Functions</a> </li>
            </details>
            <details>
                <summary>🔑🔒Maps</summary>
                <ul>
                    <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/map/hash_map.dart">1. HashMap</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/map/map.dart">2. Basic Of Map</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/map/map_examples.dart">3. Applications Of Map</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/5_maps/bin/main.dart">4. Main</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/map/unmodifiable_map.dart">5. Unmodifiable Map</a> </li>
            </details>
            <details>
                <summary>🚶🚶🚶🚶Queue</summary>
                <ul>
                    <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/queue_bdo/queue.dart">1. Queue Implementation</a> </li>
            </details>
            <details>
                <summary>📚Stack</summary>
                <ul>
                    <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/stack_bdo/stack.dart">1. Stack Implementation</a> </li>
            </details> 
            <details>
                <summary>🦄Set</summary>
                <ul>
                    <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/set/hash_set.dart">1. HashSet</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/Data_Structure/set/set.dart">2. Basic Of Set</a> </li>
            </details>
            <details>
                <summary>🔁Iterable</summary>
                <ul>
                    <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/iterable.dart">1. Iterable</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/1_Basics/7_Collections/bin/lazy_iterable.dart">2. Lazy Iterable</a> </li>
            </details>                                                
        </ul> 
    </details>            
    </ul>

</details>
<details>
     <summary>👩🏽‍💻Dart : Object Oriented Programming</summary>
      <ul>
      <details>
        <summary>🥉Basics Of OOP</summary>
        <ul>
            <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Class_basics/bin/class.dart">1. Basics Of Class Hyerarchy</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/contructor/bin/constructors.dart">2. Constructor</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/like_a_class/bin/like_a_class.dart">3. Like A Class</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/like_a_class/bin/main.dart">4. Main</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Class_basics/bin/static_usage.dart">5- Static Keyword</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Getter_Setter/bin/Main.dart">6- Getter and Setter</a></li>
        </ul>
      </details>  
      <details>
        <summary>🥈Intermediate OOP</summary>
        <ul>
            <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Inheritance/bin/Main.dart">1. Inheritance</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Factory_Constructor/bin/factory_constructor.dart">2. Factory Constructor</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Abstract_classes/bin/abstract_classes.dart">3. Abstract Classes</a></li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Initializers/bin/initializers.dart">4. Basics Of Initializers</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Initializers/bin/final_init.dart">5- Final Initializer</a> </li>
        </ul>
      </details>
      <details>
        <summary>🥇Advance OOP</summary>
        <ul>
            <li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Interface_me/bin/interface.dart">1. Interface</a> </li><li><a href="https://github.com/bedirhantong/learn_flutter_with_me/blob/master/2_OOP/Operator_methods/bin/operator_methods.dart">2. Operator Methods</a> </li>
        </ul>
      </details>                       
      </ul>
</details>

