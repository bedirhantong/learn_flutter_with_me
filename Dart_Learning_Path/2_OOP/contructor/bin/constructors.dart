void main(List<String> args) {
  Car audi = Car();
  audi.run();
  audi.faster();
  Car wolkswogen = Car.from(2000, "Wolkswogen");
  Car hyundai = Car.from(2000, "Wolkswogen");
  print(
      "color is ${wolkswogen.color} model is: ${wolkswogen.model} car is created");
}

class Car {
  int model = 2000;
  String color = "dark";

  // Default constructor w/o parameter
  Car() {
    print("A new car is created");
  }

  /*
  - Eğer default constructor'ın içeriği boş ise şöyle de tanımlayabilirsin
  Car();
  */

  /*
    - Default constructor dışında bir constructor oluştururken direkt aynı isim
      ile oluşturamazsın. Aşağıda gösterdiğim örnek gibi bir farklılık 
      oluşturman gerekiyor (.from dan bahsediyorum)
    - Parametreyi this.x = x yapmak yerine direk parametreye this.x yazınca
      kendisi eşitliyor. 
  */
  Car.from(this.model, this.color);

  void faster() {
    print("It is getting faster");
  }

  void run() {
    print("It is running");
  }
}
