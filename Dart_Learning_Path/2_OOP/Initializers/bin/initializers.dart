void main(List<String> args) {
  FootballTeam footballTeam = FootballTeam("Fatih", 1905);
}

class Name extends Object {
  Name() : super();
  /*
    - Yukarıdaki yapı initalizer list olarak geçiyor. Orada super class dışında
    başka şeyler de kullanabilirdik. ':' dan sonra virgüllerle ayırarak
    yazabilirdik. super constructor her zaman en sona yazılır bu sıralamada.
    =>  Name() :  x() , y() , super() {}
    =>  Name() :  x() , y() , super() ;

    Peki initializer kısmında neler yapabiliriz?
    - Class içindeki değişkenlerin atamasını yapabiliriz.
        =>  Name() :  x=2 , y=45 , super() {}
  */

}

class Team {
  String name = "";
  int champ_cup = 0;
  Team(this.name, this.champ_cup) {
    print("team class' constructor is running");
  }
}

class FootballTeam extends Team {
  String boss_name = "";
  int init_year = 0;
  FootballTeam(String boss_name, int init_year)
      : this.boss_name = boss_name,
        this.init_year = init_year,
        super("GalataSaray", 22) {
    print("Football Team constructor is running\n" +
        "Name: $name" +
        "BOSS: $boss_name\n" +
        "initialize year : $init_year");
  }
}
