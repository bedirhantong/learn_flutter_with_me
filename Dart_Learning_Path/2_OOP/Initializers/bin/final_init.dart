void main(List<String> args) {
  /*
    - main fonksiyonunda final ve var ile ilk seferde değer atamak zorunda 
  değilsin. Lakin bir class içinde bulunursan ilk seferde initialize etmek 
  zorundasın. 
    - Dart için bir class içinde tanımlanan her değişken için (private hariç)
    getter ve setter oluşturulur demiştik. Final ile tanımlananlar için sadece
    getter oluşturu arkaplanda.
    - Aşağıda class B örneğinde constructor içinde initialize yaptığımız için
    final keyword'ü ile tanımlanan bir değişkene ilk değer ataması yapmak
    zorunda kalmadık.
    - Aşağıda class C kısmında da initializer list kullanıp initialize edildiği
    için ilk değer ataması yapmak zorunda kalmadık.
  - var ile tanımlama yaparken hem main fonksiyonunda hem de herhangi bir class
  içinde bir değere atamasını tanımlandığı satırda yapmak zorunda değilsin.
    - 
  */

  // final int x;
  // x = 3;

  D d = D(1, 5);
}

class A {
  final int x = 32;
  var y;
}

class B {
  final int x, y; // decleration part

  B(this.x, this.y);
}

class C {
  final int a, b;
  C(int a, int b)
      : this.a = a,
        this.b = b;
}

class D {
  int y = 0, z = 0;

  D(y, z) {
    print(this.y);
    print(this.z);
    //OUTPUT : 0 0
    this.y = y;
    this.z = z;
  }
}
