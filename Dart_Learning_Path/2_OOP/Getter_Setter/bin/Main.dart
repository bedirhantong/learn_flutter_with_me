main() {
  Student student1 = Student(12, "Ali");
  student1.age = 19;
  student1.name = "Ali"; //setter

  print(student1.age);
  print(student1.graduate_year);
  print(student1.get_salary);

  print(student1._salary);
  student1._salary = 1500;
}

class Student {
  String name = "";
  int age = 0;
  int _salary = 0;
  /*
    - Private olan değişken veya methoda aynı dosya içinde olduğumuz ve aynı 
      kütüphane içinde olduğumuz için getter setter olmadan da erişebiliriz.
      Ama aynı paket içinde dahi olsak farklı bir kütüphanedeyken getter ve 
      setter ile erişmek zorundayız.
      - Mesela aynı klasör altında Main2.dart dosyası açıp Student objesi
      oluşturduğumuzu varsayalım. _salary değişkenine getter ve setterlar 
      olmadan erişemeyiz (Main.dart'ı import etsek dahi olmaz.)
      Ayrıca setter'ı olan bir değişkeni artık 
      obje.degisken = 15 gibi değerini değiştirebilirsin, illaki metod olarak 
      çağırıp öyle değiştirmek zorunda olduğunu düşünme.
      - Genel mantık olarak eğer getter ve setterlar'ı kendin yazmak istersen
      private yap(başına _ koyarak)
  */

  Student(this.age, this.name);
  /*
    - Değişken oluşturulduğu gibi dart bizim yerimize getter ve setter yazar,
    arkaplanda. Peki kendimiz getter ve setter oluşturmak istersek ne yapmalı?

  */
  int get graduate_year => age + 2015;
  set set_salary(int salary) => this._salary = salary;
  get get_salary => this._salary;
}
