void main(List<String> args) {
  Triangle triangle = Triangle(4, 10, 1, 2);
  triangle.print_Values();
  triangle.print_Me();
}

abstract class Shape {
  final int x, y;
  Shape(this.x, this.y);
  get area;
  get perimeter;

  // no method body means you're defining an abstract method
  // abstract methods must be overridden in implementation classes
  print_Values();

  // abstract classes _can_ implement some functionality.
  // when the method functionality is written on the abstract class,
  // the implementation subclasses don't have to override it.
  void print_Me() => print("Don't need to implement me");

// So, any class that _implements_ an abstract class is guaranteed to have
// overriden the methods and properties on the abstract class or interface,
// which makes it 'type safe'.
}

class Triangle extends Shape {
  int high = 0;
  int base = 0;
  Triangle(this.base, this.high, int x, int y) : super(x, y);
  @override
  // TODO: implement area
  get area => (high * base) / 2;

  @override
  // TODO: implement perimeter
  get perimeter => base * 3;

  @override
  print_Values() {
    // TODO: implement print_Values
    print("High: $high\nBase: $base");
  }
}
