int num = 4;
int _privateVar = 19;
/* dart dilinde bir değişken özel karakter( _ ) ile başlarsa sadece
  tanımlandığı dosyada kullanılabilir.
*/

void print_to_screen(int num) {
  print(num);
}

int get_privateVar() {
  return _privateVar;
}

int calc(int num) {
  return num % 2;
}
