import 'like_a_class.dart' as commands;
// like_a_class.dart içindeki komutları commands(isteğe bağlı bir ad)
// ön eki ile kullanabiliriz demek bu

void main(List<String> args) {
  commands.print_to_screen(commands.num);
  print(commands.calc(21) == 1);
  print(commands.get_privateVar());
}
