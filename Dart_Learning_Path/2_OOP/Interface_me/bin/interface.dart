main() {
  //  Interface (Implements Class)

  var program = Program();
  program.toplam = 9;

  print(program.toplam);

  program.sesi_ac();
  program.birmetod();
}

class Arayuz {
  int toplam = 9;

  void sesi_ac() {
    print("Ses opened...");
  }

  void sesi_kapat() {
    print("Ses closing...");
  }
}

class KucukArayuz {
  void birmetod() => print("Küçük arayüz'ün bir metodu");
}

class Program implements Arayuz, KucukArayuz {
  void sesi_ac() => print("Programin sesi openning");
  void sesi_kapat() => print("Programin sesi closing");

  int toplam = 0;

  void birmetod() => print("Programin bir metodu");
  void ozel() => print("Arayüzler'den bagimsiz metod");
}

//  Abstract ile Implements arasındaki fark

abstract class A {
  get x;
  get y;
}

abstract class B {
  get a;
  get b;
  normal();

  int degisken = 0;
  ozel() => print("merhaba");
}

  /*
    - class Deneme extends A {}  örneğinde A classı abstract olduğu için getter
    metodlarını override etmek zorunda kalacaktık.
    - class Deneme implements A {}  örneğinde implement kelimesini kullandığımız
    için A classı içindeki her şey override edilmek zorunda
    - class Deneme extends B {} örneğinde B classını extend ettiğimiz için
    B classına ait getter'lar ve normal() metodunu override etmek zorundayız.
    - class Deneme implements B {} örneğinde B classı ile implement ettiğimiz için
    B classına ait her şeyi override etmek zorundayız, B classına ait olan
    bir şeyin abstract olup olmadığına bakılmaksızın override edilir. 
  */

