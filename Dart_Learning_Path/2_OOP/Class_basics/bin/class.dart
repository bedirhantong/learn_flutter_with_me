void main(List<String> args) {
  /* Commands classını kullanabilmemiz için obje oluşturmamız lazım. 
    Instance of class
    - var obje = new Commands();
      new kullanımı eskide kaldı, new olmadan sadece Commands(); yazabilirsin.
      Örnek oluşturma yöntemleri topluca
    - var obje = new Commands();
    - var obje = Commands();
    - Commands object = Commands();
    - Commands object = new Commands();
  */
  var obje = Commands();
  print(obje.num);
  obje.num = 10;
  print(obje.num);
  obje.print_to_screen("Naber");
}

class Commands {
  //Instance variable
  int num = 9;

  //method
  void print_to_screen(String variable) {
    print(variable);
  }
}
