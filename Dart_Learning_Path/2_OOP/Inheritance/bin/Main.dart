void main(List<String> args) {
  Formula1 formula1 = Formula1.Detailed("124", 200);
  formula1.run();
}

class Vehicle {
  int speed = 0;
  String model = "";

  Vehicle() {}
  Vehicle.Detailed(this.model, this.speed);
  void run() {
    print("${this.model} is running on Vehicle class");
  }
}

class Formula1 extends Vehicle {
  String mark = "";

  /*
        Super constructor kullanımı
    - Çalışma sırası şu şekilde:
    1- Constructor parantezleri
    2- Super constructor
    3- varsa Curly Braces içi (Body kısmı)

    Hadi şimdi sub-class ve super-classı kullanarak farklı constructor
    tanımlamalarını görelim.
  */

  Formula1() : super() {
    print("Empty constructors");
  }
  Formula1.Detailed(String model, int speed) : super.Detailed(model, speed) {
    print("non-empty constructors");
  }

  Formula1.Type3(String model, int speed) : super() {
    print("Type3 non-empty,super is empty constructors");
  }

  //Method overriding
  @override
  void run() {
    print("${this.model} is running on Formula1 class");
    super.run();
  }
}
