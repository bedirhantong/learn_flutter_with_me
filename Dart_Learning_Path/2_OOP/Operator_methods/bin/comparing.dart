void main(List<String> args) {
  Kimlik k1 = Kimlik("sirac", "yilmaz", 1);
  Kimlik k4 = Kimlik("sirac", "yilmaz", 1);
  Kimlik k2 = Kimlik("mirac", "yilmaz", 2);
  Kimlik k3 = k1;
  print(k1 == k2);
  print(k1 == k3);
  print(k1 == k4);
  print(k2 == k3);

  print(k1 is Kimlik);
  print(k1.runtimeType);
}

class Kimlik {
  String ad = "";
  String soyad = "";
  int no = 0;

  Kimlik(this.ad, this.soyad, this.no);

  bool operator ==(other) => no == (other as Kimlik).no;
}
