void main(List<String> args) {
  Ozel ozel1 = Ozel(1, 3);
  Ozel ozel2 = Ozel(2, 4);
  Ozel ozel4 = Ozel(2, 4);
  Ozel ozel3 = ozel1 + ozel2;
  // print("x: ${ozel3.x} y: ${ozel3.y}");

  print(ozel1 == ozel2); // false
  print(ozel2 == ozel4); // true
  print("Ozel4 hashcode : " + ozel4.hashCode.toString());
  print("Ozel2 hashcode : " + ozel2.hashCode.toString());
}

class Ozel {
  final num x, y;
  Ozel(this.x, this.y);

  /*
    - Burada  other = ozel2 oldu.
    Amacımız iki nesne toplanınca her iki nesnenin x değerlerini ve y 
    değerlerini toplayıp yeni oluşturduğumuz nesneye aktarmak.
  */
  Ozel operator +(other) => Ozel(x + (other as Ozel).x, y + (other).y);
  Ozel operator -(other) => Ozel(x - (other as Ozel).x, y - (other).y);
  Ozel operator *(other) => Ozel(x * (other as Ozel).x, y * (other).y);

  /*
    - Peki eşitlik operatörünü kullansaydık nasıl olurdu? Burada as Ozel 
    yazmasaydık other objesini Object classının bir örneği olarak varsaydığından
    x değerinin olmadığını söyleyen bir hata verirdi. İlk kısımda cast ettiğimiz
    için sağ tarafında da cast etmeye gerek kalmadı.
  */
  bool operator ==(other) => x == (other as Ozel).x && y == (other).y;
}

class Object {
  bool operator ==(other) => identical(this, other);
  /*
    - Identical fonksiyonu iki nesnenin hashcodelarına göre bir karşılaştırma
    yapar. Hashcode : Dart dilinin her bir nesneye verdiği bir kimlik 
    numarasıdır ve program her çalıştırıldığında her nesnenin hashcode değeri
    değişir, sabit kalmaz. Eğer biz
    var variable2 = variable1;
    gibi bir eşitleme yaparsak iki değişkenin hashcodelarını eşitlemiş oluruz.
   */
  external int get hashCode;
  external String toString();
  // external function = abstract function in not abstract classes
  external noSuchMethod(Invocation im);
  external Type get runtimeType;
}
