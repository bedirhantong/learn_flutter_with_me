void main(List<String> args) {
  Iterable iterable = Iterable.generate(5);
  print(iterable);

  Iterable iterable2 = Iterable.generate(9, (element) {
    print("the element is : $element");
    return element;
  });

  print(iterable2);
}
