void main(List<String> args) {
  // Map<Ke,Value>

  var people = {
    //Keys : Values
    "Musa": 2001,
    "Serhan": 1998,
    "Deza": 2002,
    "Bedirhan": 2000,
  };

  /*
  Her key uniquedir, yani map içinde aynı keyler olamaz, ama value'ler olabilir.
  */
  print("${people['Serhan']}"); // OUTPUT: 1998

  people.entries.forEach((element) {
    print("${element.key} ${element.value} yılında doğmuştur.");
  });
  /*
  Musa 2001 yılında doğmuştur.
  Serhan 1998 yılında doğmuştur.
  Deza 2002 yılında doğmuştur.
  Bedirhan 2000 yılında doğmuştur.
  */

  // Map içinde Liste Kullanımı
  Map<String, dynamic> apartman = {
    'Kat sayisi': 5,
    'Daire sayisi': 10,
    'Boş daireler': 4,
    'Boş daire numaraları ': [1, 5, 6]
  };
  List<int> bos_daireler = apartman['Boş daire numaraları '];
  print(bos_daireler); // OUTPUT : [1, 5, 6]

  // Peki liste içinde Map yaparsak?

  List<Map<String, int>> list = [
    {'yaş': 22},
    {'boy': 176}
  ];

  print(list); // OUTPUT: [{yaş: 22}, {boy: 176}]

  print(list[0]); // OUTPUT: {yaş: 22}
  print(list[0]['yaş']); //OUTPUT : 22

  // Boş bir map oluşturma
  var araba1 = {};
  Map araba2 = {};

  var araba3 = Map();
  Map araba4 = Map();
  Map araba5 = new Map();

  // Belirli bir type vererek oluşturma

  var arabam1 = <String, int>{};
  var arabam3 = Map<String, int>();

  Map<String, int> arabam2 = {};
  Map<String, int> arabam7 = <String, int>{};
  Map<String, int> arabam4 = Map();
  Map<String, int> arabam5 = new Map();
}
