import 'dart:collection';

void main(List<String> args) {
  /*
  - Önce nasıl unmodifable list yaptığmızı hatırlayalım ondan sonra, benzer
    uygulamayı map için yapalım.
    var list2 = [1, 2, 3, 4];
    - UnmodifiableListView'e parametre olarak liste göndermemiz gerektiğinden
      yukarıdaki gibi bir liste tanımladık.
    var list = UnmodifiableListView(list2);
  */

  Map<String, int> tarih = {
    "İstanbul'un Fethi": 1453,
    "Cumhuriyet'in ilanı": 1923,
    "Fransız İhtilali": 1789
  };

  var unmodif_map = UnmodifiableMapView(tarih);
  print(unmodif_map);

  unmodif_map["My Birthday"] = 2000;
  /*
    Yukarıdaki gibi bir ekleme yapamayız çünkü unmodif_map ekleme ve çıkarmaya
    kısacası düzenlemeye(var olan bir key değerinin value'sini bile 
    değiştiremezsin) izin vermez.
  */
}
