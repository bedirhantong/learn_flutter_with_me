void main(List<String> args) {
  Map<String, int> map = Map.fromIterables(['elma', 'armut', 'muz'], [1, 2, 3]);
  // print(map);

  /*
    .putIfAbsent() metodu eğer girilen key değeri yoksa ekleme yapar.
  */
  map.putIfAbsent('elma', () => 23);
  map.putIfAbsent('mandalina', () => 4);
  // print(map); // OUTPUT : {elma: 1, armut: 2, muz: 3, mandalina: 4}

  map['elma'] = 10;
  // print(map); //{elma: 10, armut: 2, muz: 3, mandalina: 4}

  var key = 'mandalina';
  // print("$key adlı key, map'te var mı : ${map.containsKey(key)}");
  // OUTPUT : mandalina adlı key, map'te var mı : true

  var value = 33;
  // print("$value değeri map'te var mı : ${map.containsValue(value)}");
  // OUTPUT : 33 değeri map'te var mı : false
}
