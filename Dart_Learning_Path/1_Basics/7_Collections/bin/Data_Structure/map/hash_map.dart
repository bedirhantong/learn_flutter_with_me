import 'dart:collection';

void main(List<String> args) {
  /* 
  - map oluştururken Map.fromIterables metodunu da kullanabiliriz
    - bu metod 2 bölümden oluşur. Birincisi keyler diğeri value değerleridir.
  */

  var map = Map.fromIterables(["first", "second", "third", "fourth"],
      ["bedo", "Musa", "deziş", "Serhan"]);
  print(
      map); // OUTPUT: {first: bedo, second: Musa, third: deziş, fourth: Serhan}
  print(map["first"]); //OUTPUT: bedo
  map["sonuncu"] = "whoKnows";
  print(map);
  // OUTPUT: {first: bedo, second: Musa, third: deziş, fourth: Serhan, sonuncu: whoKnows}
  map["besinci"] = "besinciyim";
  print(map);
  // OUTPUT: {first: bedo, second: Musa, third: deziş, fourth: Serhan, sonuncu: whoKnows, besinci: besinciyim}
  /* 
    Görüldüğü gibi eklenme sırasına göre sıra korundu. hashMap'lerde 
    bu sıra korunmaz. Ama map'lere göre daha performanslıdır.
    - hashmap kullanabilmemiz için dart:collection import edilmeli.
  */
  HashMap hashMap =
      HashMap.fromIterables(["bedo", "Musa", "deziş", "Serhan"], [1, 2, 3, 4]);
  print(hashMap);
  //OUTPUT: {first: bedo, second: Musa, third: deziş, fourth: Serhan}
  hashMap['ronaldo'] = 7;
  print("\nHashmap: ");
  print(hashMap);
  //OUTPUT:{first: bedo, sonuncu: whoknows, second: Musa, third: deziş, fourth: Serhan}
  hashMap['messi'] = 10;
  print("\n$hashMap");
}
