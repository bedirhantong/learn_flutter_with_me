import 'dart:collection';

void main(List<String> args) {
  // hem eleman sayısını hem de elemanları değiştirilemeyen listedir.

  /// once normalde nasıl oluyordu onu görelim, daha sonra ek bir kütüphane ile
  /// oluşturulabilen unmodifiable listemizi oluşturalım.
  var list = List.from([1, 2, 3, 5]);
  list.add(2354);
  print(list);

  var degistirilemez = UnmodifiableListView(list);
  /*
  1- dart:collection  kütüphanesini kullanmalıyız.
  2- Bu tipte bir liste oluşturmak için parametre olarak başka bir liste 
  göndermeliyiz.
  3- Değiştirilemez bir kütüphane olduğu için add, remove vb. metodlar 
  işlevsizdir. 
  4- İşlevsiz metodlar kullanıldığı takdirde compile time'da değil, runtime'da
  hata alırsın.
  */
  degistirilemez.add(123);
  degistirilemez[0] = 351;
  /*
  Yukarıdaki ornekte sanki calısır gibi duruyor ama calıstırdığında runtime'da
  hata alacaksın.
  */
}
