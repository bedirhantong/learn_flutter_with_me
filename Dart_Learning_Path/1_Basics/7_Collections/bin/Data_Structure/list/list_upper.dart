void main(List<String> args) {
  //Geçen derste neler ogrendik tekrar edelim
  List<int> myList = <int>[];

  myList.add(12);
  myList
    ..add(45)
    ..add(125)
    ..add(35);
  myList.addAll([24, 353, 324]);

  print(myList.isEmpty);
  print(myList);

  print("Last member that is removed is ${myList.removeLast()}");
  myList.insertAll(1, [1, 2, 3, 4, 5]);
  print(myList);

  String c = "To be or I|O not to be";
  print(c.split(" "));

  /*
  Burada split metodu ile bosluklardan ayırarak yeni bir liste dondurduk
  c.split(" ") kısmını bir liste olarak dusunursek c.split(" ")[0] sıfırıncı 
  elemanını temsil eder
  */
  List nList = c.split(" ");
  print(c.split(" ")[0]);
  print(nList[0]);

  List n2List = c.split(" ")[3].split("|");
  print(n2List);

  /*
  Bir üstteki örnekte Stringten listeye geçişi gördük. Peki ya tersi?
  String myStr = myList.join(" ");
  join sayesinde, liste elemanları arasında gönderdiğimiz parametreyi ekleyerek
  String döndürdük.
  */
  String myStr = nList.join("-");
  print(myStr);

  /*
  Her eleman özelinde void olarak işlem yapıyormuşuz gibi düşünebilirsin. Yani 
  aslında void bir metod içerisindeyiz ve element parametresi ile özgürce
  işlem yapabilirsin.
  */
  nList.forEach((element) {
    print("We also have $element in the list!");
  });

  print("Liste : " + nList.toString());
  /*
  foreach gibi ama verilen bu sefer metodumuz boolean gibi düşüneceksin ve 
  return statement var. Yani çıktı olarak her eleman için bir kontrol 
  yapman gerekiyor.
  */
  print(nList.every((element) => element[0] == "t" || element[0] == "k"));
  // elemanlardan hepsi t veya k ile baslıyorsa ona gore true,false dondurur

  print(nList.any((element) => element[0] == "t" || element[0] == "k"));
  // elemanlardan herhangi biri t veya k ile baslıyorsa ona gore true,false
  // dondurur

  List<int> nums = [1, 2, 3, 4, 5, 6, 7];
  print(nums.reduce((value, element) => value + element));
  /*
  Buradaki aslında anonim bir fonksiyon var. Listenin türüne göre değişir
  reduce metodunun tipi. liste elemanları ile ard arda islem yapmamızı
  saglar. İki parametremiz var. 
  value: bir onceki islemin sonucudur. İlk basta sıfırdır.
  element: liste icindeki her bir elemanıdır.
  Yukarıdaki örnekte listedeki elemanların toplamını bulmak istedik.
  */
  print("Liste elemanlari tek mi cift mi kontrol edelim");
  nums.forEach((element) {
    if (element.isEven)
      print("$element is even");
    else
      print("$element is odd");
  });

  // nums.forEach((element) {
  //   if (element.isOdd)
  //     print("$element is odd");
  //   else
  //     print("$element is even");
  // });
}
