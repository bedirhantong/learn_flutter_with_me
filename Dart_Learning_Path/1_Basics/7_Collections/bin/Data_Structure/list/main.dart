void main(List<String> args) {
  var students = new List.filled(3, "null");
  print(students);
  students[0] = "Ahmet";
  students[1] = "Mehmet";
  students[2] = "Cenk";
  print(students);
  /* students.add("bedirhan"); yaparsak hata alırız çünkü 
  zaten başta listenin boyutunu belirttik...
  */
  var sehirler = ["Ankara", "istanbul", "Antalya"];
  sehirler.add("Diyarbakir");
  print(sehirler);
  print("sehirler uzunluk : " + sehirler.length.toString());
  print("sehirler tersten : " + sehirler.reversed.toString());
  print("ilk sehir : " + sehirler.first.toString());
  print("son sehir : " + sehirler.last.toString());

  // hadi antalya ile diyarbakır arasına bingöl koyalım
  sehirler.insert(3, "Bingol");
  print(sehirler);
  print(sehirler.firstWhere((element) => element.contains("a")));
}
