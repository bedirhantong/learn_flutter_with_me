void main(List<String> args) {
  ///Fixed lenght lists
  dynamic myList = new List.filled(10, 0);
  for (var i = 0; i < myList.length; i++) {
    myList[i] = i;
  }

  var list = [];
  list.add(10);
  list.add(12);
  list.add(124);
  list.insert(0, 2);
  list.insert(1, 3);
  list[0] = 1;

  // list.replaceRange(0, 3, []);

  // print("\nElement of list is following");
  // for (var element in list) {
  //   print(element);
  // }
  // print("the first : ${list.first}");

  //  list.fillRange(0,list.length,);

  // list.add(124);
  // list[0] = 1;
  // print("\nElement of list is following");
  // for (var element in list) {
  //   print(element);
  // }

  // list.removeRange(0, list.length);
  // print("Element of list is following");
  // for (var element in list) {
  //   print(element);
  // }

  List myList2 = [];
  List<int> myList3 = <int>[];

  // print("myList2 is empty : "+myList2.isEmpty.toString());

  myList2
    ..add(3)
    ..add(54)
    ..add(23)
    ..add(56)
    ..add(25)
    ..insert(0, 1);
  // print(myList2);

  myList2.remove(54);
  print("myList2'den ${myList2.removeLast()} elemani cikarildi");
}
