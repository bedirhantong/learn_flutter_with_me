void main(List<String> args) {
  /*
  - Set veri yapısı listelere benzer. Farkı listeler gibi aynı elemanı birden 
  fazla bulundurmazlar. Her eleman uniquedir, yalnızca bir kere bulunur.
  - Listeden set'e toSet() metodu ile de geçebilirsin.

          Farklı set tanımlama yöntemleri
    - Set<int> nSet = Set();
    - var numbers = Set<int>.from([1, 2, 3, 4, 5]);
    - var emptySet = Set<int>();
    - var list = [1, 2, 3, 3, 4, 5, 5];
      
      var set = list.toSet();
    - Dart 2.2 ile birlikte {} ile de set tanımlayabiliriz. 
      Yani {}   görünce aklımıza map gelmeyecek sadece. setler için de kullanılabildiğini görmüş olduk.
      
      var set1 = <String>{}; 
      Yukarıdaki tanımlama bir map olamaz çünkü value değeri yok.

      Set<String> set2 = {};
      Yukarıdaki tanımlama bir map olamaz çünkü value değeri yok. Eğer bir map olsaydı        var map = {};                             olarak tanımlanması gerekirdi.

      var set3 = {'a','b','c'};
      Yukarıdaki tanımlama bir map olamaz çünkü value değeri yok. Eğer map olsaydı            var set3 = {'a':1,'b':2,'c':3};           gibi olması gerekirdi.
  */

  var list = [1, 2, 3, 3, 4, 5, 5];
  var set = list.toSet();
  print("list: $list");
  print("set: $set");
  // İkinci bir set tanımlama yöntemi
  Set<int> nSet = Set();
  nSet.add(32);
  nSet.add(32);
  nSet.add(332);
  nSet.add(3);
  nSet.add(2);
  print(nSet);

  // Ucuncu bir set tanımlama yöntemi
  var numbers = Set<int>.from([1, 2, 3, 4, 5]);

  // Dorduncu bir set tanımlama yöntemi
  var emptySet = Set<int>();
  emptySet
    ..add(32)
    ..add(235)
    ..add(2)
    ..add(46);

  // İki set'in ortak elemanlarını bulup, set döndüren metoda bakalım.

  var set1 = emptySet.intersection(numbers);
  print(set1.toList()); // liste olarak
  print(set1); // set olarak
  /*
  ikisinde aynı elemanlara bakarken önce hangisini yazdığımızın 
  bir önemi yok. Sonuç ikisinde de aynı çıkar
  */

  var fark = emptySet.difference(numbers);
  print(fark.toList()); // liste olarak
  print(fark); // set olarak
  print(
      "\nempty'de olup numbers'da olmayanlar (liste olarak): ${fark.toList()}");
  print("empty'de olup numbers'da olmayanlar (set olarak) : $fark");

  /*
  ikisinde farlı elemanlara bakarken önce hangisini yazdığımızın 
  bir önemi var. Aslında bakmak istediğimiz şey ilk sette olup ikincide 
  olmayan elemanlar, o yüzden farklı çıkar sonuçlar.
  */

  var fark1 = numbers.difference(emptySet);
  print(
      "\nnumbers'da olup empty'de olmayanlar (liste olarak): ${fark1.toList()}");
  print("numbers'da olup empty'de olmayanlar (set olarak): $fark1");
}
