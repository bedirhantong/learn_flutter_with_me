import 'dart:collection';

void main(List<String> args) {
  /*
  - Kullanabilmek için dart:collection kütüphanesi import edilmeli
  - Yine eşsiz elemanlara sahip bir setimiz var.
  - Set'ten farkı elemanlar girildiği sıraya göre yazılmaz.
  - Set'te veriler girildiği sıraya göre yazılır, eğer bu önemli değilse
  veri yapısı olarak hashset'i hız ve verimlilik açısından kullanmamız 
  daha mantıklı olacaktır.
  - null değere izin verir.
  - Hashset her ne kadar hız olarak treeset ve setten daha iyi olsa da
  hafıza olarak treeset daha verimlidir.
  */
  var hash_set = HashSet.from([1, 2, 1, 2, "String", null, null, []]);
  print(hash_set);
}
