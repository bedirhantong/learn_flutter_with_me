import 'dart:collection';

void main(List<String> args) {
  /*
  - Yine bir iterable yapısıdır.
  - dart: collection import edilmek gerekiyor.
  - İlk giren ilk çıkar mantığı vardır.
  - Silme işlemi baştan, ekleme işlemi sondan yapılır(add() metodu ile).
  - Set veya Liste yapısında başa ekleme, baştan silme veya sona ekleme ve
    sondan silme gibi metodları yoktur.
  - toSet veya toList ile daha sonradan liste veya set yapısı elde edebilirsin.
  */

  var my_queue = Queue.from([1, 2, 3, "string", null, null, []]);
  my_queue.addFirst(1);
  my_queue.addLast(10);
  print(my_queue);
  my_queue.removeFirst();
  my_queue.removeLast();
  print(my_queue);
  print(my_queue.elementAt(1));
  my_queue.add(11);
  print(my_queue);
}
