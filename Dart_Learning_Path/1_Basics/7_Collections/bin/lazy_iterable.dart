void main(List<String> args) {
  /*
  - Lazy iterable metodları, iterable bir yapı üzerine uygulanan ve sonuç
    olarak iterable bir yapı döndüren metodlardır.
  */

  /* .where() : Iterable'daki her eleman için ayrı ayrı true veya false bir
     sonuç döndürür. true sonuçlar ile yeni bir iterable oluşturur.
  */
  List<int> nums = [5, 17, 23, 51];
  var result = nums.where((element) {
    if (element > 20)
      return true;
    else
      return false;
  });
  // OUTPUT : (23, 51)
  print(result);

  Iterable iterable = nums.where((nums_element) {
    print("Kontrol ediliyor: $nums_element");
    return nums_element.isOdd;
  });
  /*
Kontrol ediliyor: 5
Kontrol ediliyor: 17
Kontrol ediliyor: 23
Kontrol ediliyor: 51
(5, 17, 23, 51)
  */
  print(iterable);

//  .expand() amacı aslında tek bir eleman yerine birden fazla eleman eklemedir.
//  OUTPUT : [siyah, kırmızı, beyaz, sarı, mavi]
  List renkler = ['siyah', 'beyaz', 'mavi'];
  var expand = renkler.expand((renk) {
    return renk == 'beyaz' ? ['kırmızı', 'beyaz', 'sarı'] : [renk];
  });
  /*
    return ['kırmızı', 'beyaz', 'sarı']; deseydik her eleman yerine bu 3 
    elemanı eklerdi.
  */
  print(expand.toList());

/*  .map(): iterable üzerinde değişiklik yapıyor. Aslında hepsi için değişiklik
    yapıyor. Eğer else yazmasaydık mesela output (kırmızı,null,yeşil) olurdu.
    Yani bütün elemanları kapsayan bir değişiklik gerektiğinde .map() kullanırız
    OUTPUT : (kırmızı, gri, yeşil)
*/
  List renklerim = ['siyah', 'beyaz', 'mavi'];
  var map = renklerim.map((renk) {
    if (renk == 'siyah') return 'kırmızı';
    if (renk == 'mavi')
      return 'yeşil';
    else
      return 'gri';
  });
  print(map);

/*  .take() ilk (parametre olarak girilen) değer kadar elemanı listenin başından
    alıp yeni bir listeye atar ve o listeyi döndürür.
    OUTPUT: (12, 24, 77, 45, 64)
*/
  List sayilar = [12, 24, 77, 45, 64, 85, 93, 100];
  print(sayilar.take(5));

/*  .takeWhile() : .take() metoduna benzer şekilde n tane elemana sahip
    bir liste döndürür ama sunduğu koşula uygun olan elemanları alır. 
    Ve bir özelliği de şu; sayilar listemizde mesela 77 70 sayısından büyük ve 
    biz 70'den küçük sayıları arıyoruz. .takeWhile() koşulu sağlamayan ilk 
    elemana kadar bakar. Yani 77den sonra 45 mesela 70 den küçük ama listeye 
    eklenmez.
    OUTPUT: (12, 24)
*/
  print(sayilar.takeWhile((n) {
    if (n < 70)
      return true;
    else
      return false;
  }));

/*  .skip() : .take() aksine parametre olarak girilen ilk n değeri atlar,
    daha sonraki elemanları bir listeye atıp, o listeyi döndürür.
    OUTPUT: (85, 93, 100)
*/
  print(sayilar.skip(5));

/*  .skipWhile() : .takeWhile() gibi çalışır. .skip() metodunun koşullu 
    versiyonu diyebiliriz. 70'den küçük olan elemanları almayacak gibi
    gözüküyor başta. Koşulu sağlamayan ilk elemanda koşula bakmayı bırakıp,
    döndürülecek listenin içine eklemeye başlar.
    OUTPUT: (77, 45, 64, 85, 93, 100)
*/
  print(sayilar.skipWhile((n) {
    if (n < 70)
      return true;
    else
      return false;
  }));
}
