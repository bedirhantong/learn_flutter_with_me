void main(List<String> args) {
  // conditionals
  var num;
  num = -2;
  // var = "abc";   bu hata verir
  if (num >= 20) {
    print("Yeah it is greater than 20");
  } else if (num < 20 && num > 0) {
    print("0 ile 20 arasında");
  } else {
    print("Negatif");
  }

  var note = "A";

  switch (note) {
    case "A":
      {
        print("A aldınnn");
      }
      break;
    case "A":
      {
        print("B aldınnn");
      }
      break;
    default:
      {
        print("Alamadın");
      }
      break;
  }
}
