void main(List<String> args) {
  bool isTrue = false;
  var num = isTrue ? 1 : 0;
  print("num is true : " + num.toString());

  bool isNotTrue = true;
  var value = isNotTrue ? 1 : 0;
  print("value is not true : $value");

  /// It is also allow that you can use paranthesees before and after the
  /// ternary condition expression
  var num1 = 45;

  /// there are 2 different usage for that
  print("\nThe first usage is just print it anyway w\/o assigning it ");
  (num1 > 46 ? print("46 dan buyuktur") : print("46 dan kucuktur"));
  print("\nThe second usage is assign it to a var value");
  var result =
      (num1 > 46 ? print("46 dan buyuktur") : print("46 dan kucuktur"));

  /// There is just a different one to looking it if it is null or not
  /// (... ?? ...)
  /// It say if left side of question mark is null then do the right side of
  /// question mark, if it is not null, assing null to that variable
  /// This is acctually null aware operation
  dynamic question = (null ?? 'How are you');
  print(question);
  question = ('23' ?? 'Im doing fine hwbu?');
  print(question);
}
