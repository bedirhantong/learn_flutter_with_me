void main(List<String> args) {
  // print_clearly("merhaba");
  // print("Result : " + mathPower(2, 3).toString());

  // test("ahmet", "mehmet");

  test2("bir", c: "iki", b: "uc");

  test2("merhba");

  test3();

  print(test4());
}

int test4() => 4;

void test3() => print("Test3");

void print_clearly(String string) {
  print(string + " printed");
}

int mathPower(int base, int power) {
  int result = 1;
  for (var i = 0; i < power; i++) {
    result *= base;
  }
  return result;
}

void pow(a, b, c) {
  print(a);
  print(b);
  print(c);
}

void test(a, [b, c]) {
  print(a);
  print(b);
  print(c);
}

void test2(a, {b, c}) {
  print(a);
  print(b);
  print(c);
}
