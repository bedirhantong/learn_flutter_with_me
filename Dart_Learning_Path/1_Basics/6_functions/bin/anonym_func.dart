void main(List<String> args) {
  /*
  int m(int dividing, int divider) {
    return dividing ~/ divider;
  }
  Normalde yukarıdaki gibi olan fonksiyonun adına eşittir koyup,
  curly braceslarından sonra ; koyduğumuzda ve tabiki main fonksiyonun 
  içinde yapacağız, bu şekilde olan bir tanımla anonim fonksiyon olarak geçer.
  Dönüş tipindeki bir değişkene atıyoruz.   
  Ve var usage yerine Function olarak tanmlayabiliriz tipini. Daha doğru bir
  kullanım olur.
  */

  Function div = (int dividing, int divider) {
    return dividing ~/ divider;
  };

  Function sub = (int num1, int num2) {
    return num1 - num2;
  };

  print(div(12, 3));
  print(sub(12, 3));

  /// Daha da kısaltmak istersek fat arrow kullanabiliriz.

  Function mul = (int num1, int num2) => num1 * num2;
  print(mul(12, 3));
}
