void main(List<String> args) {
  String message = "New Message";
  print("Message : ${message.length}");

  for (var i = 0; i < message.length; i++) {
    print(message[i]);
  }

  print("\nReplaceAll");
  String newStr = message.replaceAll("e", "a");
  print(newStr);

  print("\nReplaceFirst");
  String newStr1 = message.replaceFirst("e", "a");
  print(newStr1);
  print("\nToCase");
  print(message.toLowerCase());
  print(message.toUpperCase());

  print("\nSubstring");
  print(message.substring(6));
  print(message.substring(0, 6));

  print("\nSag sol bosluklari siler, trim");
  print(" merhaba ".trim());
  print("\nol bosluklari siler, trim");
  print(" merhaba ".trimLeft());
  print("\nSag bosluklari siler, trim");
  print(" merhaba ".trimRight());

  print("\n Ceil");
  num numara = 3.2;
  print("3.2 ceil is : ${numara.ceil()}");
  print("\n Floor");
  num numara1 = 3.2;
  print("3.2 floor is : ${numara1.floor()}");

  print("\nDouble round");
  double variable = 3.4;
  print("$variable round is : ${variable.round()}");
  double variable1 = 3.5;
  print("$variable1 round is : ${variable1.round()}");

  print("\nDouble to Int");
  print("$variable to int is : ${variable.toInt()}");

  bool result = true;
  print("result is : " + result.toString());
}
