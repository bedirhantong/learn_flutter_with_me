void main(List<String> args) {
  // Const : Compile Time (Derlenme Zamanı)
  // Final : Run time (Çalışma Zamanı)
  /* 
    - const değeri initialize edildiğinde atanmak zorunda ve sonradan
    değiştirilemez
    - final ile tanımlarsak ilk değer atamak zorunda değiliz. Başka bir 
    satırda ilk değer ataması yapabiliriz
    - İkisi de ilk değer ataması yapıldıktan sonra değiştirilemez.


    - İllaki const double num =4; şeklinde tanımlamak zorunda değiliz.
    Final ve const keywordlerini var gibi düşünebiliriz. Bu olay Javada yok.
    Yani final x = 45;  veya const num = 4; gibi tanımlamalar mümkün.

    - PEKİ NE ZAMAN CONST NE ZAMAN FİNAL?
        - Eğer bir değer runtime'a geçilmeden önce de belirgin ise 
        o zaman bizim const kullanmamız gerekiyor. Mesela Pi değeri bundan 
        yıllar önce de 3.14 idi o yüzden derlenme zamanında da belli ne olduğu,
        o yüzden const kullanıp runtimeda bir daha o kod kısmını çalıştırmayıp
        dart'a zaman kazandırabiliriz. Böylece program daha hızlı çalışacak.
        - Mesela bize o kodun çalıştırıldığı anın tarihi lazım. Bu zamanla 
        değişen bir şey olduğunda runtime'a kadar belirsiz bir şey. O yüzden 
        kesinlikle final olarak tanımlanmalı. 

  */

  const double pi = 3.14;
  final double n;
  n = 32;

  final num = 3;

  final time = DateTime.now();
  print("$time\'da calistirildi.");
}
