void main(List<String> args) {
  dynamic myVariable = 42;
  myVariable = 'hello'; // OK

  var myVariable1; // defaults to dynamic
  myVariable1 = 42; // OK
  myVariable1 = 'hello'; // OK

  Object? val = 42;
  val = 44.23;
  val = "Hiii!";
  val = false;
}
