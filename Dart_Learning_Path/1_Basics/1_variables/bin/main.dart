import 'dart:ffi';

void main(List<String> args) {
  //numbers,strings,bool

  String name = "Bedirhan";
  print("Merhaba ben " + name);

  // int,double
  int age = 2;
  double number_double = 23.5;

  /*
  dart type safe bir dildir yani string ile int veya double bir 
  değeri + ile print içinde yazarsan direk toplama algılar 
  bu işleme izin vermez. 
  */
  print("age : " + age.toString());
  print("number : " + number_double.toString());

  bool cond = age == 2;

  if (cond) {
    print("yes it is " + age.toString());
  } else {
    print("nooooo it is not 2");
  }

  String differentUsage = """
  Ben 
  Bedirhan
  Naber""";

  print(differentUsage);

  num number = 4.5;
  num number2 = 5;
  print("number : " + number.toString());
  print("number2 : " + number2.toString());
  print("-------------------");
  print("number : $number");
  print("---------------");
  print("number2 : ${number2}");

  // different usage of var
  var number3 = 1.42e5;
  print(number3);

  double value3 = .34;
  print(value3);

  num someNumber = 3;
  final newNum = someNumber as int;
  final nNum = someNumber.toDouble();
  final nnNumber = someNumber.toInt();
  print(newNum);
  print(nNum);
  print(nnNumber);
}
