void main(List<String> args) {
  var message = 'Hello' + ' my name is ';
  const name = 'Ray';
  message += name; // 'Hello my name is Ray'

  final my_message = StringBuffer();
  my_message.write("Hello ");
  my_message.write("name is ");
  my_message.write(" Bedirhan!");
  print(my_message.toString());
}
