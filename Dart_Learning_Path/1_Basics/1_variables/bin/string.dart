void main(List<String> args) {
  String greeting1 = 'hello, world';
  String confirm = 'Press to Confirm';
  String userName = 'WallaceTheCat';

  print("Hello," + " World");

  print("Hello" "," " World");

  var age = 29;
  print("My age is $age");
  // My age is 29

  print("Next year I'll be ${age + 1}");
  // Next year I'll be 30

  String greeting = 'howdy';

  print(greeting.split(""));
  // ['h', 'o', 'w', 'd', 'y']

  assert(greeting.length == 5); // true

  print(greeting.indexOf('o'));
  // 1

  print(greeting.contains("hat"));
  // false

  var greet = "hello man";
  greet = "hello ma'm";
  print(greet);

  /*
  Dart strings are a collection of UTF-16 code units. 
  UTF-16 is a way to encode Unicode values by using 16-bit numbers. 
  If you want to find out what those UTF-16 codes are, you can do it like so:
  This will print the following list of numbers in decimal notation:
   */
  var salutation = 'Hello!';
  print(salutation.codeUnits); // [72, 101, 108, 108, 111, 33]

  const dart = '🎯';
  print(dart.codeUnits); // [55356, 57263]

  print("my cat's food");
  print('my cat\'s food');
  print('I \u2764 Dart\u0021'); // I ❤ Dart!
  print('I love \u{1F3AF}'); //I love 🎯
}
