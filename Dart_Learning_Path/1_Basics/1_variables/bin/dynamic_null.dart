void main(List<String> args) {
  var deneme; // dynamic
  print(deneme);

  var deneme2 = ' '; // String
  print(deneme2);

  var deneme3 = ''; // String
  print(deneme3);

  dynamic musa = "Musa";

  print(musa);
  musa = 22;
  print(musa);
  musa = 22.9;
  print(musa);
}
