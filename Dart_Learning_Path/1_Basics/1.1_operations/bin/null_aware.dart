void main(List<String> args) {
  var x = null;

  /// assign if x is null then assign x to 9
  x ??= 9;
  print(x);
  // or
  // int a = 5;
  // a ??= 3;
  // print(a);

  var y = null;
  var num = null;

  ///assign num to y, if y is null then assign num to 14
  num = y ?? 14;
  print(num);
}

  // class User {
  //   var userAge;

  //   void setUserAge(User user) {
  //     /// if user.userAge either is null or not, assign it anyway
  //     /// Just don't throw exception
  //     this.userAge = user?.userAge;
  //   }
  // }
