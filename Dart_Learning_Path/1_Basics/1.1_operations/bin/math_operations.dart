import 'dart:math';

void main(List<String> args) {
  print(sqrt(16));

  print(pi);

  print(cos(135 * pi / 180));
  print(atan(135));
  print(sin(135 * pi / 180));
  print(tan(45));
  print(acos(145));

  print(max(4, 10));
  print(min(4, 2));
  print(max(sqrt(16), pi * 2));

  /// Converts [x] to a [double] and returns the natural exponent, [e],
  print(exp(16));

  /// Converts [x] to a [double] and returns the natural logarithm of the value.
  ///
  /// Returns negative infinity if [x] is equal to zero.
  /// Returns NaN if [x] is NaN or less than zero.
  print(log(90));

  /// Notice that the result may overflow. If integers are represented as 64-bit
  /// numbers, an integer result may be truncated, and a double result may
  /// overflow to positive or negative [double.infinity].
  print(pow(2, 3));
}
