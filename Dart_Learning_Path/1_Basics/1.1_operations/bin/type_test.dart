void main(List<String> args) {
  int num = 5;
  bool isInt = num is int;
  print("is num int : $isInt");

  bool isString = num is! String;
  print("isn\'t num a String value, right? : $isString");
}
