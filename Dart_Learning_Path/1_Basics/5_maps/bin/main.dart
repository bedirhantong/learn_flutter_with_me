void main(List<String> args) {
  var vocabulary = {"book": "kitap", "table": "masa", "pencil": "kalem"};
  print(vocabulary);

  print("\nvalue of book is : " + vocabulary["book"].toString());
  print("\n");

  for (var element in vocabulary.entries) {
    print(element.key.toString() + " : " + element.value.toString());
  }

  print("\nAdding element\n");
  vocabulary["name"] = "isim";

  for (var element in vocabulary.entries) {
    print(element.key.toString() + " : " + element.value.toString());
  }
  vocabulary["name"] = "ad";

  print("\nChanging de value of the name key\n");

  for (var element in vocabulary.entries) {
    print(element.key.toString() + " : " + element.value.toString());
  }

  // vocabulary.remove("name");

  // if (vocabulary.containsKey("name")) {
  //   print("Name isn't removed");
  // } else {
  //   print("the key name is already removed");
  // }

  // vocabulary.clear();
  // if (vocabulary.isEmpty) {
  //   print("It is empty");
  // } else {
  //   print("it is not empty yet");
  // }

  print("\n\nKeys : Values\n");
  vocabulary.forEach((key, value) {
    print(key.toString() + " : " + value.toString());
  });

  print("\n\nKeys : Values\n");
  vocabulary.forEach((k, v) => print(k.toString() + " : " + v.toString()));
}
