void main(List<String> args) {
  /*
    Eğer for döngüsü içinde liste üzerinde düzenleme yapmaycaksan for in 
    döngüsü kullanman daha mantıklı, aksi takdirde diğer döngülere başvur.
  */
  List<int> list = [1, 2, 3, 4, 5, 6];
  for (var element in list) {
    if (element.isEven) {
      print("$element is even");
    } else {
      print("$element is odd");
    }
  }
}
