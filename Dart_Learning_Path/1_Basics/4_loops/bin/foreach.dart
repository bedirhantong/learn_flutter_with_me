void main(List<String> args) {
  List<int> nums = [1, 2, 3, 5, 77, 89];
  nums.forEach((element) {
    if (element.isEven) {
      print("$element is even");
    } else {
      print("$element is odd");
    }
  });
}
