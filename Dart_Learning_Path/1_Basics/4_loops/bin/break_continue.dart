void main(List<String> args) {
  int num = 0;
  while (num < 10) {
    if (num % 2 == 0) {
      num++;
      continue;
    }
    print(num);
    num++;
  }
}
