void main(List<String> args) {
  for (var i = 0; i < 10; i++) {
    print(i);
  }

  var students = ["ahmet", "mehmet", "salih"];

  print("For");
  for (var student in students) {
    print(student);
  }

  print("\nWhile");
  var num = 5;
  while (num <= 10) {
    print(num);
    num++;
  }

  print("\nDowhile");
  var num2 = 5;
  do {
    print(num2);
    num2++;
  } while (num2 <= 10);
}
